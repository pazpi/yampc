# Simple controll for mpd
In order this program to work you need to export this envarionment variable: `MPD_HOST` and `MPD_PORT`

```
export MPD_HOST=192.168.1.100
export MPD_PORT=6600
```

## Command:
`toggle`
Stop and play music

`prev`
Re-play the same song if the songs is haed by 10 sec, otherwise play previus song

`next`
Play next song in the playlist

`[+,-][0,100]`
Increase or decrese volume, the specific number can be omitted, a default value of 10 will be used

## Dependency
libmpdclient v2.10

## Build
just run `make` and then copy the executable yampc whereever you want inside
`$PATH`
