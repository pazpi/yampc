// main.c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//libmpdclient
#include <mpd/client.h>
#include <mpd/player.h>
#include <mpd/status.h>
// max time in seconds for repeat the same songs
#define MAXTIME 10
// how much each step increase/decrease volume
#define STEPVOLUME 5

void fatal(char* message){
    char error_message[100];
    strcpy(error_message, "[!!] Fatal Error ");
    strncat(error_message, message, 83);
    perror(error_message);
    exit(-1);
}

bool usage(char*);
bool nextSong();
bool prevSong();
bool toggleSong();
bool setVolume(char*);

int main(int argc, char** argv){
    
    if(argc>3||argc==1) usage(argv[0]); 
    char* argument = argv[1];

    if(strcmp(argument,"next") == 0) {
        nextSong();
    }
    if(strcmp(argument,"prev") == 0) {
        prevSong();
    }
    if(strcmp(argument,"toggle") == 0) {
        toggleSong();
    }
    if ((argument[0] == '+') || (argument[0] == '-')){
        setVolume(argument);
    } 
    return 0;
}

bool nextSong(){
    struct mpd_connection *conn;

    conn = mpd_connection_new(NULL, 0, 0);
    if(mpd_connection_get_error(conn) != MPD_ERROR_SUCCESS){
        fatal("Connection failed!");
    }

    mpd_send_next(conn);
    mpd_connection_free(conn);
    return 0;
}

bool prevSong(){
    struct mpd_connection *conn;
    struct mpd_status *status;

    conn = mpd_connection_new(NULL, 0, 0);
    if(mpd_connection_get_error(conn) != MPD_ERROR_SUCCESS){
        fatal("Connection failed!");
    }
    mpd_send_status(conn);
    status = mpd_recv_status(conn);
    if(status == NULL) { fatal("status is NULL!"); }

    unsigned elapsed_time = mpd_status_get_elapsed_time(status);

    if(elapsed_time < MAXTIME){
        mpd_send_previous(conn);
    }
    else{
        mpd_send_seek_pos(conn, 0, 0);
    }

    mpd_status_free(status);
    mpd_connection_free(conn);
    return 0;
}

bool toggleSong(){
    struct mpd_connection *conn;

    conn = mpd_connection_new(NULL, 0, 0);
    if(mpd_connection_get_error(conn) != MPD_ERROR_SUCCESS){
        fatal("Connection failed!");
    }
    mpd_run_toggle_pause(conn);
    mpd_connection_free(conn);
    return 0;
}

bool setVolume(char* argument){
    struct mpd_connection *conn;
    struct mpd_status *status;
    char sign = argument[0];
    int vol = atoi(++argument);
    int curr_vol;

    conn = mpd_connection_new(NULL, 0, 0);
    if(mpd_connection_get_error(conn) != MPD_ERROR_SUCCESS){
        fatal("Connection failed!");
    }
    mpd_send_status(conn);
    status = mpd_recv_status(conn);

    curr_vol = mpd_status_get_volume(status);


    if(vol == 0){
        vol = MAXTIME;
    }

    if(sign == '+'){
        if((curr_vol + vol) >= 100){
            mpd_send_set_volume(conn, 100);
        } else{
            mpd_send_set_volume(conn, vol+curr_vol);
        }
    } else
    {
        if((curr_vol - vol) <= 0){
            mpd_send_set_volume(conn, 0);
        } else{
            mpd_send_set_volume(conn, curr_vol-vol);
        }
    }

    mpd_status_free(status);
    mpd_connection_free(conn);

    return 0;
}

bool usage(char* nameProgram){
    printf("Simple mpd controller based on libmpdclient library\n");
    printf("usage: %s [ACTION]\n", nameProgram);
    printf("ACTION:\n");
    printf("\tnext: play next song in the playlist\n");
    printf("\tprev: play the same song if time is greater than 10sec, othervise play the last song in the playlist\n");
    printf("\ttoggle: Change play/pause state\n");
    printf("\t+[0-100]: increse volume by the quantity defined\n");
    printf("\t-[0-100]: decrese volume by the quantity defined\n");
    exit(1);
}
